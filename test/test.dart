

import './element_basic_test.dart';
import './dataset_basic_test.dart';
import './parse_element_test.dart';
import './parse_dataset_test.dart';
import './read_explicit_study.dart';
import './read_implicit_study.dart';

void main() {

  elementBasicTest();
  datasetBasicTest();
  parseElementTest();
  parseDatasetTest();
  readExplicitStudy();
  readImplicitStudy();

}