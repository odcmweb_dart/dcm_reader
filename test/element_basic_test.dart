import 'package:unittest/unittest.dart';
import '../lib/dataset.dart';

void elementBasicTest() {
  int tag = 0x00100010;
  int vr = VR_OB;
  int offset = 8;
  int length = 16;

  Element e0 = new Element(0x00100010, VR_OB, 8, 16);
  Element e1 = new Element(0x00080005, VR_ST, 24, 6);

  group("Element Tests", () {
    test('Element', () {

      expect(e0.tag, 0x00100010);
      expect(e0.vr, VR_OB);
      expect(e0.offset, 8);
      expect(e0.length, 16);
      expect(e1.tag, 0x00080005);
      expect(e1.vr, VR_ST);
      expect(e1.offset, 24);
      expect(e1.length, 6);

    });

  });
}
