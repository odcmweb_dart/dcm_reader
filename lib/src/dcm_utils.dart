
library dcm_utils;



String intToHex(int i, {int digits: -1, String padding: '0', String prefix: '0x'}) {
  var s = i.toRadixString(16);
  s = (digits == -1) ? s : s.padLeft(digits, padding);
  return prefix + s;
}

String tagToDicom(int tag) {
  int group = tag >> 16;
  int element = tag & 0xffff;
  var gStr = intToHex(group, digits: 4, prefix: "");
  var eStr = intToHex(element, digits: 4, prefix: "");
  return '($gStr,$eStr)';
}

class ReadError extends Error {
  String msg;

  ReadError(this.msg);

  toString() => 'ReadError: $msg';
}

readError(String msg) => throw new ReadError(msg);