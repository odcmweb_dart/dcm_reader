library dataset_implicit;

import 'dart:typed_data';

import 'package:utilities/utilities.dart';

import './constants.dart';
import './dataset_base.dart';
import './dcm_utils.dart';
import './element.dart';
import './item.dart';
import './sequence.dart';


class DatasetImplicit extends Dataset {

  DatasetImplicit([int length = 8, endianness]) : super(length, endianness);

  DatasetImplicit.fromByteArray(ByteArray buf) : super.fromByteArray(buf);

  DatasetImplicit.fromUint8List(Uint8List bytes) : super.fromUint8List(bytes);


/// All the methods in this file are used to read [datasets] that have
/// an Implicit VR.

  /// Reads an implicit data set.
  static DatasetImplicit read(ByteArray buf) {
    var ds = new DatasetImplicit.fromByteArray(buf);
    while (buf.isNotEmpty) {
      //print('buf remaining = ${buf.limit - buf.position}');
      var element = ds.readElementImplicit(buf.limit);
      ds[element.tag] = element;
    }
    return ds;
  }

  int _readVFImplicitLength() => buf.readUint32();

  // Read an *Implicit* [Element] from [buf].
  Element readElementImplicit([int limit]) {
    print('rEE: limit=$limit');
    // [tag], [length], and [offset] must be read in this order.
    int tag = readTag();
    int length = _readVFImplicitLength();
    int offset = buf.position;

    if (peekTag() == $item) {
      // Peek at the next tag to see if it is an item tag.  If so, this is a sequence.
      // This is not 100% safe because a non sequence item could have data that has
      // these bytes, but this is how to do it without a data dictionary.
      // Parse the sequence.
      if (hasUndefinedLength(length)) {
        return readSequenceImplicitUndefinedLength(tag, offset);
      } else {
        int limit = buf.getLimit(length);
        return readSequenceImplicit(tag, offset, length, limit);
      }
    } else {
      if (hasUndefinedLength(length)) {
        // vr = OB, OD, OF, OW, UN, UR, or UT (Not SQ or Item)
      length = findItemDelimitationItemAndSetElementLength(offset, tag);
      }
      buf.seek(length);
      return new Element(tag, -1, offset, length);
    }
  }

  /// Read a Sequence with known length.
  readSequenceImplicit(int tag, int offset, int length, int limit) {
    print('rSQI: ${tagToDicom(tag)}, off=$offset, len=$length, limit=$limit');
    List<Item> items = [];
    while (buf.position < limit) {
      Item item = readItemImplicit();
      items.add(item);
    }
    Sequence sq = new Sequence(tag, VR_SQ, offset, length, false, items);
    sq.debug();
    return sq;
  }

  /// Read a [Sequence] with *Undefined Length*, i.e. [vflength] == -1.
  Sequence readSequenceImplicitUndefinedLength(int tag, int offset) {
    var length;
    List<Item> items = [];
    while (buf.isNotEmpty) {
      Item item = readItemImplicit();
      // If this is the sequence delimitation item, return the offset of the next element
      if (item.tag == $sequenceDelimitationItem) {
        // This is the *Sequence Delimitation Item*, get the [sequence] length
        // and return the [Sequence].
        length = buf.getLength(item.offset);
        return new Sequence(tag, VR_SQ, offset, length, true, items);
      } else {
        items.add(item);
      }
    }
    // eof encountered - log a warning and return a [Sequence]
    // with [length] based on the [buf.limit].
    warning('eof encountered before finding "SequenceDelimitationItem" '
            'in [Sequence] of "Undefined Length" with tag=$tag');
    buf.position = buf.limit;
    length = buf.getLength(offset);
    return new Sequence(tag, VR_SQ, offset, length, true, items);
  }

  /*
  Item readItemImplicit() {
    var item = readItem();
    Dataset ds = new DatasetImplicit();
    if (hasUndefinedLength(item.length)) {
      item.hadUndefinedLength = true;
      item.dataset = readDatasetImplicitUndefinedLength();
      item.length = buf.getLength(item.offset);
    } else {
      item.dataset = read(buf);
    }
    return item;
  }
  */

  Item readItemImplicit() {
    var item = readItemHeader();
    print('rII: $item');
    if (item.length == 0) return item;
    item.dataset = new DatasetImplicit();
    while (buf.position < item.limit) {
      print('pos=${buf.position}, limit=${item.limit}');
      var element = readElementImplicit();
      item.dataset[element.tag] = element;
      if (element.tag == $itemDelimitationItem) {
        if (item.hadUndefinedLength) {
          // The item delimiter tag marks the end of this [Item]. Set the length and return [item].
          item.length = buf.getLength(item.offset);
          print('rII2:$item');
          return item;
        } else {
          throw "Encountered item delimiter in Item with explicit length";
        }
      }
    }
    if (item.hadUndefinedLength) {
      // eof encountered - log a warning and return what we have for the element
      warning('eof encountered before finding item delimeter in item of undefined length');
    }
    return item;
  }

  /*
  Item readItemImplicit() {
    var item = readItem();
    if (item.length == 0) return item;
    item.dataset = new DatasetImplicit();
    if (item.hadUndefinedLength) {
      //readDatasetImplicitUndefinedLength(item);
      while(buf.isNotEmpty) {
        var element = readElementImplicit();
        item.dataset[element.tag] = element;
         // we hit an item delimeter tag, return the current offset to mark
         // the end of this sequence item
         if (element.tag == $itemDelimitationItem) {
           // The item delimiter tag marks the end of this [Item]. Set the length and return.
           item.length = buf.getLength(item.offset);
           return item;
         }
      }
      // eof encountered - log a warning and return what we have for the element
      warning('eof encountered before finding sequence item delimeter in '
              'sequence item of undefined length');
    } else {
      while(buf.isNotEmpty) {
        var element = readElementImplicit();
        item.dataset[element.tag] = element;
        if (element.tag == $itemDelimitationItem) {
          throw "Encountered item delimiter in Item with explicit length";
        }
      }
    }
    return item;
  }

  void readDatasetImplicitUndefinedLength(Item item) {
    while(buf.isNotEmpty) {
      var element = readElementImplicit();
      item.dataset[element.tag] = element;
       // we hit an item delimeter tag, return the current offset to mark
       // the end of this sequence item
       if (element.tag == $itemDelimitationItem) {
         // The item delimiter tag marks the end of this [Item].
         // Set the length and return.
         item.length = buf.getLength(item.offset);
         return;
       }
    }
    // eof encountered - log a warning and return what we have for the element
    warning('eof encountered before finding sequence item delimeter in '
            'sequence item of undefined length');
  }
  */
}
