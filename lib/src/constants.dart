library constants;

/// DICOM constant names and values

/// Value Representations needed by Dataset
const VR_AE = 0x4145;
const VR_AS = 0x4153;
const VR_AT = 0x4154;
const VR_BR = 0x4252;
const VR_CS = 0x4353;
const VR_DA = 0x4441;
const VR_DS = 0x4453;
const VR_DT = 0x4454;
const VR_FD = 0x4644;
const VR_FL = 0x464c;
const VR_IS = 0x4953;
const VR_LO = 0x4c4f;
const VR_LT = 0x4c54;
const VR_OB = 0x4f42;
const VR_OD = 0x4f44;
const VR_OF = 0x4f46;
const VR_OW = 0x4f57;
const VR_PN = 0x504e;
const VR_SH = 0x5348;
const VR_SL = 0x534c;
const VR_SQ = 0x5351;
const VR_SS = 0x5353;
const VR_ST = 0x5354;
const VR_TM = 0x544d;
const VR_UI = 0x5549;
const VR_UL = 0x554c;
const VR_UN = 0x554e;
const VR_UR = 0x5552;
const VR_US = 0x5553;
const VR_UT = 0x5554;

const Map vrToString = const {
  0x4145: "AE",
  0x4153: "AS",
  0x4154: "AT",
  0x4252: "BR",
  0x4353: "CS",
  0x4441: "DA",
  0x4453: "DS",
  0x4454: "DT",
  0x4644: "FD",
  0x464c: "FL",
  0x4953: "IS",
  0x4c4f: "LO",
  0x4c54: "LT",
  0x4f42: "OB",
  0x4f44: "OD",
  0x4f46: "OF",
  0x4f57: "OW",
  0x504e: "PN",
  0x5348: "SH",
  0x534c: "SL",
  0x5351: "SQ",
  0x5353: "SS",
  0x5354: "ST",
  0x544d: "TM",
  0x5549: "UI",
  0x554c: "UL",
  0x554e: "UN",
  0x5552: "UR",
  0x5553: "US",
  0x5554: "UT"
};

int getExplicitVFSize(int vr) {
  return (vr == VR_OB ||
      vr == VR_OD ||
      vr == VR_OW ||
      vr == VR_OF ||
      vr == VR_SQ ||
      vr == VR_UN ||
      vr == VR_UR ||
      vr == VR_UT) ? 4 : 2;
}

// Undefined Length
const $minusOneAsUint32 = 4294967295;
bool hasUndefinedLength(int i) => i == $minusOneAsUint32;

// Transfer Syntax
const $transferSyntaxTag = 0x00020010;

// Sequence & Item Delimiters
const $sequenceDelimitationItem = 0xFFFEE0DD;
const $item = 0xFFFEE000;
const $itemDelimitationItem = 0xFFFEE00D;
const $itemDelimitationItemLength = 8; // group + element + length
