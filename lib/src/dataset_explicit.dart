library dataset_explicit;

//import 'dart:collection';
import 'dart:typed_data';

import 'package:utilities/utilities.dart';

import './constants.dart';
import './dcm_utils.dart';
import './element.dart';
import './dataset_base.dart';
import './item.dart';
import './sequence.dart';
//TODO decide on one way or the other
import '../../lib/dataset.dart';


class DatasetExplicit extends Dataset {

  //TODO figure out what the average size of a dataset is.
  DatasetExplicit([int length = 8, endianness]) : super(length, endianness);

  DatasetExplicit.fromByteArray(buf) : super.fromByteArray(buf);

  DatasetExplicit.fromUint8List(Uint8List bytes) : super.fromUint8List(bytes);

  /// Reads an explicit data set.
  static DatasetExplicit read(ByteArray buf) {
    var ds = new DatasetExplicit.fromByteArray(buf);
    while (buf.isNotEmpty) {
      //print('buf remaining = ${buf.limit - buf.position}');
      var element = ds.readElementExplicit(buf.limit);
      ds[element.tag] = element;
      //ds.elements[element.tag] = element;
    }
    return ds;
  }

  /// Read a Value Representation Code (VR).  See PS3.10.
  int _readVR() {
    int c1 = buf.readUint8();
    int c2 = buf.readUint8();
    int vrCode = c2 + (c1 << 8);
    return vrCode;
  }

  /// Read the Value Field Length
  /// Note: This is always an even number, but the last byte might be
  /// padding.  See DICOM PS3.5.
  int _readVFLength(int vr) {
    int sizeInBytes = getExplicitVFSize(vr);
    if (sizeInBytes == 4) {
      buf.seek(2);
      return buf.readUint32();
    } else {
      return buf.readUint16();
    }
  }

  // Read an *Explicit* [Element] from [buf].
  Element readElementExplicit([int limit]) {
    print('rEE: limit=$limit');
    // [tag], [vr], [length], and [offset] must be read in this order.
    int tag = readTag();
    int vr = _readVR();
    int length = _readVFLength(vr);
    int offset = buf.position;

    if (vr == VR_SQ) {
      if (hasUndefinedLength(length)) {
        return readSequenceExplicitUndefinedLength(tag, offset);
      } else {
        int limit = buf.getLimit(length);
        return readSequenceExplicit(tag, offset, length, limit);
      }
    } else {
      if (hasUndefinedLength(length)) {
        //TODO verify that this list is correct.
        // vr = OB, OD, OF, OW, UN, UR, or UT (Not SQ or Item)
        length = findItemDelimitationItemAndSetElementLength(offset, tag);
      }
      buf.seek(length);
      return new Element(tag, vr, offset, length);
    }
  }

  /// Read a [Sequence] with *Known Length*.
  Sequence readSequenceExplicit(int tag, int offset, int length, int limit) {
    print('rSQE: ${tagToDicom(tag)}, off=$offset, len=$length, limit=$limit');
    List<Item> items = [];
    while (buf.position < limit) {
      Item item = readItemExplicit();
      items.add(item);
    }
    Sequence sq = new Sequence(tag, VR_SQ, offset, length, false, items);
    sq.debug();
    return sq;
  }

  /// Read a [Sequence] with *Undefined Length*, i.e. [Sequence.length] == -1.
  Sequence readSequenceExplicitUndefinedLength(int tag, int offset) {
    var length;
    List<Item> items = [];
    while (buf.isNotEmpty) {
      Item item = readItemExplicit();
      if (item.tag == $sequenceDelimitationItem) {
        // This is the *Sequence Delimitation Item*, get the [sequence] length
        // and return the [Sequence].
        length = buf.getLength(item.offset);
        return new Sequence(tag, VR_SQ, offset, length, true, items);
      } else {
        items.add(item);
      }
    }
    // eof encountered - log a warning and return a [Sequence]
    // with [length] based on the [buf.limit].
    warning('eof encountered before finding "SequenceDelimitationItem" '
            'in [Sequence] of "Undefined Length" with tag=$tag');
    buf.position = buf.limit;
    length = buf.getLength(offset);
    return new Sequence(tag, VR_SQ, offset, length, true, items);
  }

  //TODO this can be moved to Dataset_base if we abstract DatasetExplicit & readElementExplicit
  Item readItemExplicit() {
    var item = readItemHeader();
    print('rIE: $item');
    if (item.length == 0) return item;
    item.dataset = new DatasetExplicit();
    while (buf.position < item.limit) {
      print('pos=${buf.position}, limit=${item.limit}');
      var element = readElementExplicit(item.limit);
      item.dataset[element.tag] = element;
      if (element.tag == $itemDelimitationItem) {
        if (item.hadUndefinedLength) {
          // The item delimiter tag marks the end of this [Item].  Set the length and return.
          item.length = buf.getLength(item.offset -8);
          print('rIE2:$item');
          return item;
        } else {
          throw "Encountered item delimiter in Item with explicit length";
        }
      }
      if (item.hadUndefinedLength) {
        warning('eof encountered before finding item delimeter in item of undefined length');
      }
    }
    return item;
  }

  /*
  Item readItemExplicit() {
    var item = readItem();
    if (item.length == 0) return item;
    item.dataset = new DatasetExplicit();
    if (item.hadUndefinedLength) {
      readDatasetExplicitUndefinedLength(item);
    } else {
      while(buf.isNotEmpty) {
        var element = readElementExplicit(item.limit);
        item.dataset[element.tag] = element;
        if (element.tag == $itemDelimitationItem) {
          throw "Encountered item delimiter in Item with explicit length";
        }
      }
    }
    return item;
  }
  void readDatasetExplicitUndefinedLength(Item item) {
    print('DsExUndefinedLength');
    //TODO find the length here first
    while(buf.isNotEmpty) {
      var element = readElementExplicit(buf.limit);
      item.dataset[element.tag] = element;
      if ( element.tag == $itemDelimitationItem) {
        // The item delimiter tag marks the end of this [Item].
        // Set the length and return.
        item.length = buf.getLength(item.offset);
        return;
      }
    }
    // eof encountered - log a warning and return what we have for the element
    warning('eof encountered before finding sequence delimitation item while '
            'reading sequence item of undefined length');
  }
  //TODO remove after testing
  */
/*
  void format(Formatter fmt) {
    //print('elements.length=${elements.length}');
    if (elements.length != 0) {
      fmt.writeLn('Dataset:[');
      fmt.indent();
      elements.forEach((int tag, Element e) { e.format(fmt); });
      fmt.outdent();
      fmt.writeLn(']');
    } else {
      fmt.writeLn('Dataset:[]');
    }
  }
  */
}
