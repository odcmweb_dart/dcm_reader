
library test.parse_element_test;

import 'dart:typed_data';

import 'package:unittest/unittest.dart';
import 'package:utilities/utilities.dart';

import '../lib/src/constants.dart';
import '../lib/src/dataset_explicit.dart';
import '../lib/src/element.dart';

void parseElementTest() {

  group("Element Parse Test", () {

    test("returns element", () {
      // Arrange
      var buf = new ByteArray(32);
      var dataset = new DatasetExplicit.fromByteArray(buf);

      // Act
      var element = dataset.readElementExplicit(32);
      //print('element=$element');

      // Assert
      expect(element, new isInstanceOf<Element>(), reason: "no element returned");
    });

    test("parsed tag is correct", () {
      // Arrange
      var bytes = new Uint8List(32);
      bytes[0] = 0x11;
      bytes[1] = 0x22;
      bytes[2] = 0x33;
      bytes[3] = 0x44;
      var ds = new DatasetExplicit.fromUint8List(bytes);

      // Act
      var element = ds.readElementExplicit(32);
      //print(element);

      // Assert
      expect(element.tag, 0x22114433, reason: "tag not correct");
    });

    test("parsed vr is correct", () {
      // Arrange
      var byteArray = new Uint8List(32);
      byteArray[0] = 0x11;
      byteArray[1] = 0x22;
      byteArray[2] = 0x33;
      byteArray[3] = 0x44;
      byteArray[4] = 0x53; // ST
      byteArray[5] = 0x54;
      var ds = new DatasetExplicit.fromUint8List(byteArray);

      // Act
      var element = ds.readElementExplicit(32);
      //print(element);

      // Assert
      expect(element.vr, VR_ST, reason: "tag not correct");
    });

    test("parse element for 2 byte length is correct", () {
      // Arrange
      var bytes = new Uint8List(1024);
      bytes[0] = 0x11;
      bytes[1] = 0x22;
      bytes[2] = 0x33;
      bytes[3] = 0x44;
      bytes[4] = 0x53; // ST
      bytes[5] = 0x54;
      bytes[6] = 0x01; // length of 513
      bytes[7] = 0x02;
      var ds = new DatasetExplicit.fromUint8List(bytes);

      // Act
      var element = ds.readElementExplicit(1024);

      // Assert
      expect(element.length, 513, reason: "length is not correct");
    });

    test("parse element for 4 byte length is correct", () {
      // Arrange
      var bytes = new Uint8List(16909060 + 12);
      bytes[0] = 0x11;
      bytes[1] = 0x22;
      bytes[2] = 0x33;
      bytes[3] = 0x44;
      bytes[4] = 0x4F; // OB
      bytes[5] = 0x42;
      bytes[6] = 0x00;
      bytes[7] = 0x00;
      bytes[8] = 0x04; // 4    overall length = 16909060 = (16777216 + 131072 + 768 + 4)
      bytes[9] = 0x03; // 768
      bytes[10] = 0x02; // 131072
      bytes[11] = 0x01; // 16777216
      var ds = new DatasetExplicit.fromUint8List(bytes);

      // Act
      var element = ds.readElementExplicit(16909060 + 12);

      // Assert
      expect(element.length, 16909060, reason: "length is not correct");
    });

    test("parse element has correct data offset", () {
      // Arrange
      var bytes = new Uint8List(16909060 + 12);
      bytes[0] = 0x11;
      bytes[1] = 0x22;
      bytes[2] = 0x33;
      bytes[3] = 0x44;
      bytes[4] = 0x4F; // OB
      bytes[5] = 0x42;
      bytes[6] = 0x00;
      bytes[7] = 0x00;
      bytes[8] = 0x04; // 4    overall length = 16909060 = (16777216 + 131072 + 768 + 4)
      bytes[9] = 0x03; // 768
      bytes[10] = 0x02; // 131072
      bytes[11] = 0x01; // 16777216
      var ds = new DatasetExplicit.fromUint8List(bytes);

      // Act
      var element = ds.readElementExplicit(16909060 + 12);

      // Assert
      expect(element.offset, 12, reason: "dataOffset is not correct");
    });

  });

}
