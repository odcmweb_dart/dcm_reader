library date;


class Date {
  int _year, _month, _day;

  Date(this._year, this._month, this._day);

  int get year => _year;
  int get month => _month;
  int get day => _day;

  bool operator ==(Date d) => _year == d.year && _month == d.month && _day == d.day;

  toString() => '$_year-$_month-$_day';
}
