
library dataset_basic_test;

import 'dart:typed_data';
import 'package:unittest/unittest.dart';
import 'package:utilities/utilities.dart';

import '../lib/dataset.dart';


ByteArray makeTestData() {
  var elements = [// x22114433          US         4          0xadde 0x1234
    [0x11, 0x22, 0x33, 0x44, 0x55, 0x53, 0x04, 0x00, 0xde, 0xad, 0x34, 0x12],
        // x22114434          OB                    4                    "O\B"
    [
        0x11,
        0x22,
        0x34,
        0x44,
        0x4F,
        0x42,
        0x00,
        0x00,
        0x04,
        0x00,
        0x00,
        0x00,
        0x4F,
        0x5C,
        0x42,
        0x00],
        // x22114435          DS                    10                    " 1.2\2.3  "
    [
        0x11,
        0x22,
        0x35,
        0x44,
        0x4F,
        0x42,
        0x00,
        0x00,
        0x0A,
        0x00,
        0x00,
        0x00,
        0x20,
        0x31,
        0x2E,
        0x32,
        0x5C,
        0x32,
        0x2E,
        0x33,
        0x20,
        0x20],
        // x22114436          IS         2          "1.2\2.3"
    [0x11, 0x22, 0x36, 0x44, 0x49, 0x53, 0x04, 0x00, 0x31, 0x32, 0x33, 0x34],
        // x22114437          DA         8          "20140329"
    [
        0x11,
        0x22,
        0x37,
        0x44,
        0x49,
        0x53,
        0x08,
        0x00,
        0x32,
        0x30,
        0x31,
        0x34,
        0x30,
        0x33,
        0x32,
        0x39],
        // x22114438          TM         14         "081236.531000"
    [
        0x11,
        0x22,
        0x38,
        0x44,
        0x49,
        0x53,
        0x0E,
        0x00,
        0x30,
        0x38,
        0x31,
        0x32,
        0x33,
        0x36,
        0x2E,
        0x35,
        0x33,
        0x31,
        0x30,
        0x30,
        0x30,
        0x20],
        // x22114439          PN         10         "F^G^M^P^S"
    [
        0x11,
        0x22,
        0x39,
        0x44,
        0x50,
        0x4E,
        0x0A,
        0x00,
        0x46,
        0x5E,
        0x47,
        0x5E,
        0x4D,
        0x5E,
        0x50,
        0x5E,
        0x53,
        0x20],
        // x2211443a          ST         4         " S  "
    [0x11, 0x22, 0x3A, 0x44, 0x50, 0x4E, 0x04, 0x00, 0x20, 0x53, 0x20, 0x20]];

  var arrayLength = 0;
  elements.forEach((element) {
    arrayLength += element.length;
  });

  //print('length=$arrayLength');

  var bytes = new Uint8List(arrayLength);
  var index = 0;
  elements.forEach((element) {
    for (var i = 0; i < element.length; i++) {
      bytes[index++] = element[i];
    }
  });

  //print('bytes=$bytes');

  return new ByteArray.fromUint8List(bytes);
}

void datasetBasicTest() {

  group("Basic Dataset Tests", () {
    test("DataSet uint16", () {
      // Arrange
      ByteArray buf = makeTestData();
      //print(buf);
      var dataset = DatasetExplicit.read(buf);
      // Act
      int uint16 = dataset.uint16(0x22114433);

      // Assert
      expect(uint16, equals(0xadde), reason: "uint16 returned wrong value");
    });

    test("DataSet uint16", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var uint16 = dataset.uint16(0x22114433, 1);

      // Assert
      expect(uint16, equals(0x1234), reason: "uint16 returned wrong value");
    });

    test("DataSet uint32", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var uint32 = dataset.uint32(0x22114434);

      // Assert
      expect(uint32, equals(0x00425C4F), reason: "uint32 returned wrong value");
    });

    test("DataSet numStringValues", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var numStringValues = dataset.numStringValues(0x22114434);

      // Assert
      expect(numStringValues, equals(2), reason: "numStringValues returned wrong value");
    });

    test("DataSet string", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var str = dataset.string(0x2211443a);

      //print('str="$str"');

      // Assert
      expect(str, equals('S'), reason: "string returned wrong value");
    });

    test("DataSet text", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var text = dataset.text(0x2211443a);

      // Assert
      expect(text, equals(' S'), reason: "text returned wrong value");
    });

    test("DataSet string with index", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var str = dataset.string(0x22114434, 1);
      //print('str="$str"');

      // Assert
      expect(str, 'B', reason: "string returned wrong value");
    });

    test("DataSet floatString", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var float = dataset.floatString(0x22114435, 0);

      // Assert
      expect(float, equals(1.2), reason: "floatString returned wrong value");
    });

    test("DataSet floatString", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var float = dataset.floatString(0x22114435, 0);

      // Assert
      expect(float, equals(1.2), reason: "floatString returned wrong value");
    });

    test("DataSet floatString no index", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var float = dataset.floatString(0x22114435);

      // Assert
      expect(float, equals(1.2), reason: "floatString returned wrong value");
    });

    test("DataSet floatString", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var float = dataset.floatString(0x22114435, 0);

      // Assert
      expect(float, equals(1.2), reason: "floatString returned wrong value");
    });

    test("DataSet intString", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var val = dataset.intString(0x22114436);

      // Assert
      expect(val, equals(1234), reason: "intString returned wrong value");
    });

    test("DataSet date", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var val = dataset.date(0x22114437);

      // Assert
      expect(val.year, equals(2014), reason: "date returned wrong value");
      expect(val.month, equals(3), reason: "date returned wrong value");
      expect(val.day, equals(29), reason: "date returned wrong value");
    });

    test("DataSet time", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var val = dataset.time(0x22114438);

      // Assert
      expect(val.hour, equals(8), reason: "time returned wrong value for hours");
      expect(val.minute, equals(12), reason: "time returned wrong value for minutes");
      expect(val.second, equals(36), reason: "time returned wrong value for seconds");
      expect(
          val.fraction,
          equals(531000),
          reason: "time returned wrong value for fractionalSeconds");
    });

    test("DataSet personName", () {
      // Arrange
      ByteArray buf = makeTestData();
      var dataset = DatasetExplicit.read(buf);

      // Act
      var val = dataset.personName(0x22114439);

      // Assert
      expect(val.family, equals('F'), reason: "personName returned wrong value for familyName");
      expect(val.given, equals('G'), reason: "personName returned wrong value for givenName");
      expect(val.middle, equals('M'), reason: "personName returned wrong value for middleName");
      expect(val.prefix, equals('P'), reason: "personName returned wrong value for prefix");
      expect(val.suffix, equals('S'), reason: "personName returned wrong value for suffix");
    });

  });

}
