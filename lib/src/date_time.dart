

library reader.date_time;

class DateTime {
  int year, month, day;
  int hour, minute, second, fraction;

  DateTime(this.year, this.month, this.day, this.hour, this.minute, this.second, this.fraction);

  toString() => '$year-$month-${day}T$hour:$minute:$second.$fraction';
}
