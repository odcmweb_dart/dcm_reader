library reader.time;

class Time {
  int _hour, _minute, _second, _fraction;

  Time(this._hour, this._minute, this._second, this._fraction);

  int get hour => _hour;
  int get minute => _minute;
  int get second => _second;
  int get fraction => _fraction;

  bool operator ==(Time t) =>
      _hour == t.hour && _minute == t.minute && _second == t.second && _fraction == t.fraction;

  toString() => '$_hour:$_minute:$_second.$_fraction';
}
