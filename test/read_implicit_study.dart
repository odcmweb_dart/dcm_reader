
library test.read_implicit_study;

import 'dart:io';
import 'dart:typed_data';
import 'package:utilities/formatter.dart';

import '../lib/dataset.dart';

const explicitStudy = "D:/acr_sfd_data/334/0000000A";

//void main() {
void readImplicitStudy() {
    File file = new File(explicitStudy);
    Uint8List bytes = file.readAsBytesSync();
    Dataset ds = readDataset(bytes);
    Formatter fmt = new Formatter();
    ds.format(fmt);
    fmt.flush();
}
