

library reader.sequence;

import 'package:utilities/formatter.dart';

import './constants.dart';
import './dcm_utils.dart';
import './element.dart';
import './item.dart';

class Sequence extends Element {
  bool hadUndefinedLength;
  List<Item> items;

  Sequence(int tag, int vr, int offset, int length, this.hadUndefinedLength, this.items)
      : super(tag, vr, length, offset);

  bool get hasUndefinedLength => length == $minusOneAsUint32;

  @override
  void format(Formatter fmt) {
    fmt.writeLn('${vrToString[vr]}[${tagToDicom(tag)}, off=$offset, len=$length]');
    fmt.indent();
    for(int i = 0; i < items.length; i++) {
      items[i].format(i, fmt);
    }
    fmt.outdent();
  }

  void debug() {
    print('SQ-debug: ${toString()}');
    for(Item i in items) {
      print('    $i');
      Map elements = i.dataset.elements;
      print('Element count = ${elements.length}');
      elements.forEach((int tag, var element) {
       print('      $element');
      });
    }
  }

  toString() => '${vrToString[vr]}[${tagToDicom(tag)}, off=$offset, len=$length, limit=${offset + length}]';

}