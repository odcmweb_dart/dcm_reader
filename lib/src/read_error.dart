

library dataset.read_error;

class ReadError extends Error {
  String msg;

  ReadError(this.msg);

  toString() => 'ReadError: $msg';
}

readError(String msg) => throw new ReadError(msg);