
library test.read_explicit_study;

import 'dart:io';
import 'dart:typed_data';
import 'package:utilities/formatter.dart';

import '../lib/dataset.dart';

const explicitStudy = "D:/mint_test_data/sfd/CR/PID_MINT10/1_DICOM_Original/CR.2.16.840.1.114255.393386351.1568457295.17895.5.dcm";

//void main() {
void readExplicitStudy() {
  File file = new File(explicitStudy);
  Uint8List bytes = file.readAsBytesSync();
  Dataset ds = readDataset(bytes);
  Formatter fmt = new Formatter();
  ds.format(fmt);
  fmt.flush();
}