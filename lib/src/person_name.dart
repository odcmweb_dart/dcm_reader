library reader.person_name;

class PersonName {
  String _family, _given, _middle, _prefix, _suffix;

  PersonName(this._family, this._given, this._middle, this._prefix, this._suffix);

  String get family => _family;
  String get given => _given;
  String get middle => _middle;
  String get prefix => _prefix;
  String get suffix => _suffix;

  bool operator ==(PersonName p) =>
      _family == p.family &&
          _given == p.given &&
          _middle == p.middle &&
          _prefix == p.prefix &&
          _suffix == p.suffix;

  toString() => '$_prefix $_given $_middle $_family $_suffix';
}
