library dataset_base;

import 'dart:collection';
import 'dart:typed_data';

import 'package:charcode/ascii.dart';
import 'package:utilities/utilities.dart';

import './constants.dart';
import './date.dart';
//import './date_time.dart';
import './dcm_utils.dart';
import './element.dart';
import './item.dart';
import './person_name.dart';
import './time.dart';
//import '../dataset.dart';

/**
 *
 * The DataSet class encapsulates a collection of DICOM Elements and provides various functions
 * to access the data in those elements
 *
 * Rules for handling padded spaces:
 * DS = Strip leading and trailing spaces
 * DT = Strip trailing spaces
 * IS = Strip leading and trailing spaces
 * PN = Strip trailing spaces
 * TM = Strip trailing spaces
 * AE = Strip leading and trailing spaces
 * CS = Strip leading and trailing spaces
 * SH = Strip leading and trailing spaces
 * LO = Strip leading and trailing spaces
 * LT = Strip trailing spaces
 * ST = Strip trailing spaces
 * UT = Strip trailing spaces
 *
 */

/// A DICOM dataset
class Dataset implements Maps {
  ByteArray buf;
  Map<int, Element> elements = {};
  List<String> warnings = [];

  Dataset([int length = 8, Endianness endian]) {
    if (endian == null) endian = Endianness.HOST_ENDIAN;
    buf = new ByteArray(length, endian);
  }

  Dataset.fromByteArray(this.buf, [Endianness endian]) {
    if (endian == null) endian = Endianness.HOST_ENDIAN;
  }

  Dataset.fromBuffer(ByteBuffer buffer, [int start = 0, int length, Endianness endianness]) {
    buf = new ByteArray.fromBuffer(buffer, start, length, endianness);
  }

  Dataset.fromUint8List(Uint8List bytes, [int start = 0, int length, Endianness endianness]) {
    buf = new ByteArray.fromUint8List(bytes, start, length, endianness);
  }

  Dataset.view(Uint8List bytes, [int start = 0, int length, Endianness endianness]) {
    buf = new ByteArray.fromUint8List(bytes, start, length, endianness);
  }
  // Maps primitives
  Element operator [](int tag) => elements[tag];
  void operator []=(int tag, Element value) {
    elements[tag] = value;
  }
  Iterable<int> get keys => elements.keys;
  void clear() => elements.clear();
  Element remove(int key) => elements.remove(key);

  //TODO delete
  //bool containsKey(int key) => elements.containsKey(key);
  //bool containsValue(Element value) => elements.containsValue(value);

  // Core Dataset methods

  /// Peek at next tag - doesn't move the [ByteArray.position]
  int peekTag() {
    int group = buf.getUint16(buf.position);
    int element = buf.getUint16(buf.position + 2);
    //print('grp=$group, elt=$element');
    return (group << 16) + element;
  }

  /// Read the DICOM Attribute Tag
  int readTag() {
    int group = buf.readUint16();
    int element = buf.readUint16();
    return (group << 16) + element;
  }

  //vrToString(int vrCode) => 'VR=${intToHex(vrCode, digits: 4)}';

  int getVFLimit(int vfLength) {
    int limit = buf.position + vfLength;
   // print('index=${buf.position}, vfLength=$vfLength, limit=$limit');
    return limit;
  }

  // Utility for debugging
  void printAttribute(tag, vr, value) {
    print('[${tagToDicom(tag)}, $vr, $value]');
  }

  /// [Item]s have a Uint32 length field.
  int readItemLength() => buf.readUint32();

  /// Reads the tag and length of a sequence item and returns them as an object
  ///  with the following properties:
  ///     1. The item tag,
  ///     2. The length: the number of bytes in this item or 4294967295 if undefined,
  ///     3. The offset into the byteStream of the data for this item
  Item readItemHeader() {
    int tag = readTag();
    //print('tag=${tagToDicom(tag)}');
    if (tag != $item) readError('Expected Item, but received tag =$tag instead');
    int length = readItemLength();
    int offset = buf.position;
    bool hadUndefinedLength = (hasUndefinedLength(length)) ? true : false;
    return new Item(tag, offset, length, hadUndefinedLength);
  }

  static const $itemDelimitationFirst16Bits = 0xfffe;
  static const $itemDelimitationSecond16Bits = 0xe00d;

  /// Returns the [Element.length] of an [Element] or [Item] with undefined length.
  /// The [Element.length] does not including the [$itemDelimitationItem] or the
  /// [$itemDelimitationItem] length field, which should be zero.
  int findItemDelimitationItemAndSetElementLength(int offset, int tag) {
    var maxPosition = buf.limit - $itemDelimitationItemLength;
    //print('pos=${buf.position}, max=$maxPosition');
    while (buf.position < maxPosition) {
      int groupNumber = buf.readUint16();
      if (groupNumber == $itemDelimitationFirst16Bits) {
        //print('group=${groupNumber.toRadixString(16)}');
        int elementNumber = buf.readUint16();
        if (elementNumber == $itemDelimitationSecond16Bits) {
          //print('element=${elementNumber.toRadixString(16)}');
          int length = (buf.position - 4) - offset;
          int itemDelimiterLength = buf.readUint32(); // the length
          if (itemDelimiterLength != 0) {
            warning('encountered non zero length following item delimeter at position'
                    ' ${buf.position} while reading element of undefined length with tag = $tag');
          }
          // Return the Item length
          return length;
        }
      }
    }
    //TODO should this throw an Error?
    // No item delimitation item found - issue a warning, silently set the position
    // to the buffer limit and return the length from the offset to the end of the buffer.
    warning('encountered end of buffer while looking for ItemDelimiterItem');
    buf.position = buf.limit;
    return buf.limit - offset;
  }

  readEncapsulatedPixelData(Dataset dataset, frame) {
    //print('Read Pixel Data $dataset, frame=$frame');
    if (dataset == null) {
      throw "dicomParser.readEncapsulatedPixelData: missing required parameter 'dataSet'";
    }
    if (frame == null) {
      throw "dicomParser.readEncapsulatedPixelData: missing required parameter 'frame'";
    }
    var pixelElement = dataset.elements[0x7fe00010];
    if (pixelElement == null) {
      throw "readEncapsulatedPixelData: pixel data element x7fe00010 not present";
    }
    // seek to the beginning of the encapsulated pixel data and read the basic offset table
    var ds = new Dataset.fromByteArray(buf);
    buf.seek(pixelElement.offset);
    var basicOffsetTable = readItemHeader();
    if (basicOffsetTable.tag != 'xfffee000') {
      throw "readEncapsulatedPixelData: missing basic offset table xfffee000";
    }
    // now that we know how many frames we have validate the frame parameter
    var numFrames = basicOffsetTable.length / 4;
    if (frame > numFrames - 1) {
      throw
          "readEncapsulatedPixelData: parameter frame exceeds number of frames in basic offset table";
    }
    // read all the frame offsets
    var frameOffsets = [];
    for (var frameOffsetNum = 0; frameOffsetNum < numFrames; frameOffsetNum++) {
      var frameOffset = buf.readUint32();
      frameOffsets.add(frameOffset);
    }
    // now read the frame
    var baseOffset = buf.position;
    var pixelData = readFrame(baseOffset, frameOffsets, frame);
    return pixelData;
  }

  readFrame(int baseOffset, List<int> frameOffsets, frame) {
    if (frame >= frameOffsets.length) {
      throw 'readFrame: parameter frame refers to frame not in frameOffsets';
    }

    // position the byteStream at the beginning of this frame
    var frameOffset = frameOffsets[frame];
    buf.position = baseOffset + frameOffset;

    // calculate the next frame offset so we know when to stop reading this frame
    var nextFrameOffset = buf.limit;
    if (frame < frameOffsets.length - 1) nextFrameOffset = frameOffsets[frame + 1];

    // read all fragments for this frame
    var fragments = [];
    var frameSize = 0;
    while (buf.position < nextFrameOffset) {
      var fragment = readItemHeader();
      if (fragment.tag == 'xfffee0dd') break;
      fragments.add(fragment);
      frameSize += fragment.length;
      buf.seek(fragment.length);
    }

    // if there is only one fragment, return a view on this array to avoid copying
    if (fragments.length == 1) {
      return new Uint8List.view(fragments[0].offset, fragments[0].length);
    }

    // copy all of the data from the fragments into the pixelData

    var pixelData = new Uint8List(frameSize);
    var pixelDataIndex = 0;
    for (var i = 0; i < fragments.length; i++) {
      var fragmentOffset = fragments[i].offset;
      for (var j = 0; j < fragments[i].length; j++) {
        pixelData[pixelDataIndex++] = buf.uint8List[fragmentOffset++];
      }
    }

        //console.log('read frame #' + frame + " with " + fragments.length + " fragments and " + pixelData.length + " bytes");
    return pixelData;
  }


  /// Finds the element associated with [tag], where [tag] is an int and returns
  /// an unsigned int 16 if it exists and has data.  [index] is the index of a
  /// multi-valued element.  The default index 0 if not supplied.  Returns the 16 bit
  /// unsigned integer value as an [int] or null if the attribute is not present or
  /// has data of length 0.
  int uint16(int tag, [int index = 0]) {
    var element = elements[tag];
    if ((element != null) && (element.length != 0)) {
      return buf.getUint16(element.offset + (index * 2));
    }
    return null;
  }

  /// Finds the element for tag and returns the signed 16 bit integer if it exists
  /// and has a value. Returns null if the tag is not contained in [this] or if its
  /// length is 0.
  int int16(tag, [index = 0]) {
    var element = elements[tag];
    //index = (index != null) ? index : 0;
    if (element && element.length != 0) {
      return buf.getInt16(element.offset + (index * 2));
    }
    return null;
  }

  /// Finds the [element] associated with [tag].  Returns a 32 bit unsigned integer,
  /// if it exists and has a value; otherwise, returns [null]
  int uint32(tag, [index = 0]) {
    var element = this.elements[tag];
    if ((element != null) && (element.length != 0)) {
      int offset = element.offset + (index * 4);
      //print('offset=$offset');
      return buf.getUint32(offset);
    }
    return null;
  }

  /// Finds the element for tag and returns a 32 bit signed integer if it exists
  /// and has data [index] defaults to 0 if not supplied. Returns an [int] with
  /// th value of the [int32] or null if the attribute is not present or has
  /// value of length 0.
  int int32(int tag, [int index = 0]) {
    var element = this.elements[tag];
    if ((element != null) && (element.length != 0)) {
      return buf.getInt32(element.offset + (index * 4));
    }
    return null;
  }

  /// Finds the element for tag and returns a 32 bit floating point number (VR=FL)
  /// if it exists and has a value.  [index] defaults to 0 if not supplied. Returns
  /// a [double] with the value of the [float32] or null if the attribute is not
  /// present or has value of length 0.
  double float32(int tag, [int index = 0]) {
    var element = elements[tag];
    if ((element != null) && (element.length != 0)) {
      return buf.getFloat32(element.offset + (index * 4));
    }
    return null;
  }

  /// A synonm for [float32].
  double float(int tag, [int index = 0]) => float32(tag, index);

  /// Finds the [element] with [tag] and returns a 64 bit floating point number
  /// (VR=FD) if it exists and has a value; otherwise, returns null if the attribute
  /// is not present or has a length of 0.  [index] defaults to 0 if not supplied.
  double float64(int tag, [int index = 0]) {
    var element = elements[tag];
    if ((element != null) && (element.length != 0)) {
      return buf.getFloat64(element.offset + (index * 8));
    }
    return null;
  }

  /// Returns the [int] number of string values contained in the element
  /// or null if the attribute is not present or has zero length value.
  int numStringValues(tag) {
    //final backslash = new RegExp(r'\\');
    var element = this.elements[tag];
    if ((element != null) && (element.length > 0)) {
      //print('NumStringValues: offset=${element.offset}, len=${element.length}');
      var charCodes = buf.uint8List.buffer.asUint8List(element.offset, element.length);
      var s = new String.fromCharCodes(charCodes);
      List<String> list = s.split(r'\');
      //print(list);
      return list.length;
    }
    return null;
  }

  /// Returns the [element]'s value as a [String].  If [index] is provided,
  /// the element is assumed to be multi-valued and will return the value
  /// specified by index.  [null] is returned if 1) there is no component with
  /// the specified [index], 2) it does not exist or 3) it is zero length.
  ///
  /// Use this function for VRs of type AE, CS, SH and LO
  /// The returned [String] has leading and trailing spaces removed.
  //TODO make the comment correspond to the method
  string(int tag, [int index = 0]) {
    var element = elements[tag];
    if ((element != null) && (element.length != 0)) {
      var bytes = new Uint8List.view(buf.uint8List.buffer, element.offset, element.length);
      int char = bytes[bytes.length - 1];
      //print('char=$char');
      if ((char == $nul) || (char == $space))
        bytes = new Uint8List.view(buf.uint8List.buffer, element.offset, element.length - 1);
      var s = new String.fromCharCodes(bytes);
      //print('fixedString="$s"');
      if (index >= 0) {
        List<String> values = s.split(r'\');
        for (String s in values) {
          var y = s.trimRight();
          //print('val="$s", trim="$y"');
        }
        // trim leading and trailing spaces
        //TODO verify that this returns [null] if index is too big
        var value = values[index];
        //var v = value[1].codeUnitAt(0);
        //print('trailing="$v"');
        var x = value.trim();
        return x;
      } else {
        //print('fixedString="$s"');
        // trim leading and trailing spaces
        return s.trim();
      }
    }
    return null;
  }

  /// Returns a [String] with the leading spaces preserved and trailing spaces removed.
  /// Use this function to access the value for elements with VRs of type UT, ST and LT
  String text(tag, [index = 0]) {
    var element = this.elements[tag];
    if ((element != null) && (element.length != 0)) {
      var charCodes = new Uint8List.view(buf.uint8List.buffer, element.offset, element.length);
      var fixedString = new String.fromCharCodes(charCodes);
      var s = (index >= 0) ? fixedString.split(r'\')[index] : fixedString;
      return s.trimRight();
    }
    return null;
  }

  /// Parses a string to a float for the specified index in a multi-valued element.
  /// If index is not specified, the first value in a multi-valued VR will be parsed
  /// if present. Returns a floating point number or null if not present or if index
  /// exceeds the number of values in the element.
  double floatString(int tag, [int index = 0]) {
    var element = this.elements[tag];
    if ((element != null) && (element.length != 0)) {
      var value = string(tag, index);
      if (value != null) return double.parse(value);
    }
    return null;
  }

  /// Parses a [String] to an [int] for the specified [index] in a multi-valued
  /// element. If [index] is not specified, it defaults to 0, and the first value
  /// in a multi-valued VR will be returned if present.  Returns [null] if not
  ///  present or if the [index] exceeds the number of values in the element.
  int intString(int tag, [int index = 0]) {
    var element = this.elements[tag];
    if ((element != null) && (element.length != 0)) {
      //index = (index != null) ? index : 0;
      var value = this.string(tag, index);
      if (value != null) return int.parse(value);
    }
    return null;
  }

  /// Reads and parses a [Date] formatted string (VR = DA).
  /// Returns a [Date] or [null] if not present or not 8 bytes long.
  //TODO does date have to be 8 bytes long?
  Date date(tag) {
    var value = string(tag);
    if ((value != null) && (value.length == 8)) {
      var yyyy = int.parse(value.substring(0, 4));
      var mm = int.parse(value.substring(4, 6));
      var dd = int.parse(value.substring(6, 8));
      return new Date(yyyy, mm, dd);
    }
    return null;
  }

  /// Parses a [String] with format (VR = TM) HHMMSS.FFFFFF into a [Time].
  /// Returns a [Time] with hours, minutes, seconds and fractionalSeconds
  /// or [null] if not present or does not have a length of 2.
  Time time(tag) {
    var value = string(tag);
    if ((value != null) && (value.length >= 2)) { // must at least have HH
      var hh = int.parse(value.substring(0, 2));
      var mm = value.length >= 4 ? int.parse(value.substring(2, 4)) : 0;
      var ss = value.length >= 6 ? int.parse(value.substring(4, 6)) : 0;
      //TODO should read the ".", but we just skip over it for now
      var ffffff = value.length >= 8 ? int.parse(value.substring(7, 13)) : 0;
      return new Time(hh, mm, ss, ffffff);
    }
    return null;
  }

  /// Parses a [String formatted as a Person Name (VR = PN) into a [PersonName].
  /// Returns a [PersonName] or [null] if the element is not present or has a
  /// value length of 0.
  PersonName personName(int tag, [int index = 0]) {
    var stringValue = string(tag, index);
    if (stringValue != null) {
      var stringValues = stringValue.split('^');
      var familyName = stringValues[0];
      var givenName = stringValues[1];
      var middleName = stringValues[2];
      var prefix = stringValues[3];
      var suffix = stringValues[4];
      return new PersonName(familyName, givenName, middleName, prefix, suffix);
    }
    return null;
  }

  void warning(String s) {
    print(s);
    warnings.add(s);
  }

  void format(Formatter fmt) {
    fmt.writeLn('Dataset:[');
    fmt.indent();
    elements.forEach((int tag, Element e) { e.format(fmt); });
    fmt.outdent();
    fmt.writeLn(']');
  }

}
