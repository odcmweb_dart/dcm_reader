
library dataset.item;

import 'dart:typed_data';

import 'package:utilities/formatter.dart';

import './dcm_utils.dart';
import './constants.dart';

class Element {
  //TODO turn this into a ByteData object to save space.
  ByteData bd;
  //TODO make this part of bd
  //bool hadUndefinedLength;
  static const _tagOffset    = 0;  // tag
  static const _offsetOffset = 4;  // offset
  static const _lengthOffset = 8;  // length
  static const _vrOffset     = 12; // vr

  Element.empty();

  Element(int t, int v, int off, int len) {
    bd = new ByteData(14);
    tag = t;
    vr = v;
    offset = off;
    length = len;
  }

  int  get tag => bd.getUint32(_tagOffset);
  void set tag(int value) { bd.setUint32(_tagOffset, value); }

  int  get vr => bd.getUint16(_vrOffset);
  void set vr(int vr) { bd.setUint16(_vrOffset, vr); }

  int  get length => bd.getUint32(_lengthOffset);
  void set length(int i) { bd.setUint32(_lengthOffset, i); }

  int  get offset => bd.getUint32(_offsetOffset);
  void set offset(int i) { bd.setUint32(_offsetOffset, i); }

  // -1 == 4294967295 for Uint32
  bool get hasUndefinedLength => (length == $minusOneAsUint32) ? true : false;

  void format(Formatter fmt) {
    fmt.writeLn(toString());
  }

  toString() {
    int vLengthField = getExplicitVFSize(vr);
    int overhead = (vLengthField == 4) ? 12 : 8;
    int totalSize = overhead + length;
    int endPosition = offset + length;
    return '${vrToString[vr]}[${tagToDicom(tag)}, off=$offset, len=$length, total=$totalSize, endPosition=$endPosition]';
  }
}