
library test.parse_dataset_test;

import 'dart:typed_data';
import 'package:unittest/unittest.dart';
import 'package:utilities/utilities.dart';

import '../lib/dataset.dart';

Uint8List makeTestData() {
  var bytes = new Uint8List(26);
  bytes[0] = 0x11;
  bytes[1] = 0x22;
  bytes[2] = 0x33;
  bytes[3] = 0x44;
  bytes[4] = 0x4F; // OB
  bytes[5] = 0x42;
  bytes[6] = 0x00;
  bytes[7] = 0x00;
  bytes[8] = 0x00; // length = 0
  bytes[9] = 0x00;
  bytes[10] = 0x00;
  bytes[11] = 0x00;
  bytes[12] = 0x10;
  bytes[13] = 0x22;
  bytes[14] = 0x33;
  bytes[15] = 0x44;
  bytes[16] = 0x4F; // OB
  bytes[17] = 0x42;
  bytes[18] = 0x00; // OB
  bytes[19] = 0x00;
  bytes[20] = 0x02; // length = 2
  bytes[21] = 0x00;
  bytes[22] = 0x00;
  bytes[23] = 0x00;
  bytes[24] = 0x00;
  bytes[25] = 0x00;
  return bytes;
}

void parseDatasetTest() {

  group("Dataset Parse Test", () {
    test("parse returns DataSet", () {
      // Arrange
      Uint8List bytes = makeTestData();
      var buf = new ByteArray.fromUint8List(bytes);

      // Act
      Dataset dataset = DatasetExplicit.read(buf);
      //print(dataset);

      // Assert
      expect(dataset, new isInstanceOf<Dataset>());
    });

    test("DataSet has two elements", () {
      // Arrange
      var bytes = makeTestData();
      var buf = new ByteArray.fromUint8List(bytes);

      // Act
      var dataset = DatasetExplicit.read(buf);

      // Assert
      expect(
          dataset.elements[0x22104433],
          new isInstanceOf<Element>(),
          reason: "DataSet does not contain element with tag x22104433");
      expect(
          dataset.elements[0x22114433],
          new isInstanceOf<Element>(),
          reason: "DataSet does not contain element with tag x22114433");
    });

  });

}
