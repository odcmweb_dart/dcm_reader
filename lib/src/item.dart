
library reader.item;

import 'package:utilities/formatter.dart';

import './constants.dart';
import './dcm_utils.dart';
import './dataset_base.dart';

class Item {
  int tag, offset, length;
  bool hadUndefinedLength;
  Dataset dataset;

  Item(this.tag, this.offset, this.length, this.hadUndefinedLength);

  int get limit => offset + length;

  bool get hasUndefinedLength {
    if (length == $minusOneAsUint32) {
      hadUndefinedLength = true;
      return true;
    } else {
      return false;
    }
  }

  void format(int idx, Formatter fmt) {
    fmt.writeLn('$idx: ${toString()}');
    fmt.indent();
    dataset.format(fmt);
    fmt.outdent();
  }

  toString() => 'Item [${tagToDicom(tag)}, off=$offset, len=$length, limit=${offset + length}]';

}