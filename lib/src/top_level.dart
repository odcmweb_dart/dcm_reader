/**
 * This module contains the entry point for parsing a DICOM P10 byte stream
 *
 * This module exports itself as both an AMD module for use with AMD loaders as well
 * as a global browser variable when AMD modules are not being used.  See the following:
 *
 * https://github.com/umdjs/umd/blob/master/amdWeb.js
 *
 */
library dataset_top_level;

import 'dart:typed_data';
import 'package:utilities/utilities.dart';

import './constants.dart';
import './dataset_base.dart';
import './dataset_explicit.dart';
import './dataset_implicit.dart';
import './element.dart';

readPrefix(Dataset ds) {
  ds.buf.seek(128);
  //TODO fix this
  var prefix = ds.buf.readFixedString(4);
  if (prefix != "DICM") {
    throw "parseDicom: DICM prefix not found at location 132";
  }
}

readFileMetaInfo(ByteArray buf) {
  DatasetExplicit fmi = new DatasetExplicit.fromByteArray(buf);
  readPrefix(fmi);
  //save and restore later
  int limit = buf.limit;

  // Read the group length element so we know how many bytes needed
  // to read the entire meta header
  //TODO shouldn't we check that it's in fact the fmi group length element = 0x00020000
  var groupLengthElement = fmi.readElementExplicit();
  var fmiLength = fmi.buf.getUint32(groupLengthElement.offset);
  var readFileMetaInfo = fmi.buf.limit = fmi.buf.position + fmiLength;
  fmi[groupLengthElement.tag] = groupLengthElement;
  while(fmi.buf.isNotEmpty) {
    var element = fmi.readElementExplicit();
    fmi[element.tag] = element;
  }
  // Restore limit saved above
  buf.limit = limit;
  return fmi;
}

isExplicit(Dataset fmi) {
  if (fmi.elements[0x00020010] == null)
    throw 'parseDicom: missing required meta header attribute 0002,0010';

  var transferSyntaxElement = fmi.elements[$transferSyntaxTag];
  //print(transferSyntaxElement);
  String transferSyntax =
      fmi.buf.getFixedString(transferSyntaxElement.offset, transferSyntaxElement.length);
  //print('TransferSyntax="$transferSyntax", len=${transferSyntax.length}');
  if (transferSyntax == '1.2.840.10008.1.2') { // implicit little endian
    return false;
  } else if (transferSyntax == '1.2.840.10008.1.2.2') {
    throw 'parseDicom: explicit big endian transfer syntax not supported';
  }
  // all other transfer syntaxes should be explicit
  return true;
}

mergeDatasets(Dataset fmiDataset, Dataset instanceDataset) {
  fmiDataset.elements.forEach((int tag, Element element) {
      instanceDataset.elements[tag] = element;
  });
  return instanceDataset;
}

/// This is the main entry point for the [Dataset].
/// It parses the array of bytes into a DICOM Part 3.10 object.
readDataset(Uint8List bytes) {
  var buf = new ByteArray.fromUint8List(bytes);
  var fmiDataset = readFileMetaInfo(buf);
  print(fmiDataset);
  Formatter fmt = new Formatter();
  fmiDataset.format(fmt);
  fmt.flush();
  print('isExplicit=${isExplicit(fmiDataset)}');
  print('buf=$buf');
  var ds = (isExplicit(fmiDataset)) ? DatasetExplicit.read(buf) : DatasetImplicit.read(buf);
  mergeDatasets(fmiDataset, ds);
  print(ds);
  return ds;
}



